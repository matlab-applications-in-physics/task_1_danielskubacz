%Calculator
%Author: Daniel Skubacz
%Date: 16-11-2020
%PLANCK_CONSTANT:
h=(6.63*10^-34) %[J*s]
%EULER_CONSTANT:
e=exp(1)
%EARTH_RADIUS:
R=6371 %[km]
%AVOGADRA_CONSTANT:
Na=(6.023*10^23)
%EX.1.
ANS1=h/(2*pi)
%EX.2.
ANS2=sin(45/e)
%EX.3.
ANS3=(0x0098d6)/(1.445*10^23)
%EX.4.
ANS4=sqrt(e-pi)
%EX.5.
format long
ANS5 = pi
%EX.6.
my_birthday='01-Jun-1999'
a=datenum(my_birthday)
b=now-a
ANS6=b*24
%EX.7.
ANS7=atan((e^((sqrt(7)/2)-log(R/(10^8))))/(0xaaff))
%EX.8.
ANS8=(1/4)*Na/100000
